package sk.pgl.spring.bankingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminPartServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminPartServerApplication.class, args);
	}
}
