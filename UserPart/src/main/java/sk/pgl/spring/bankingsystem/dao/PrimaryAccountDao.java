package sk.pgl.spring.bankingsystem.dao;

import org.springframework.data.repository.CrudRepository;
import sk.pgl.spring.bankingsystem.domain.PrimaryAccount;

public interface PrimaryAccountDao extends CrudRepository<PrimaryAccount, Long> {
    PrimaryAccount findByAccountNumber(int accountNumber);
}
