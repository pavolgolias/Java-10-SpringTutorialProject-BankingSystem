package sk.pgl.spring.bankingsystem.dao;

import org.springframework.data.repository.CrudRepository;
import sk.pgl.spring.bankingsystem.domain.SavingsAccount;

public interface SavingsAccountDao extends CrudRepository<SavingsAccount, Long> {
    SavingsAccount findByAccountNumber(int accountNumber);
}
