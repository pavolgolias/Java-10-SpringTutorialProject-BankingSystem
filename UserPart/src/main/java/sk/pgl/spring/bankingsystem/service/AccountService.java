package sk.pgl.spring.bankingsystem.service;

import sk.pgl.spring.bankingsystem.domain.PrimaryAccount;
import sk.pgl.spring.bankingsystem.domain.SavingsAccount;
import sk.pgl.spring.bankingsystem.domain.User;

public interface AccountService {
    PrimaryAccount createPrimaryAccount();

    SavingsAccount createSavingsAccount();

    void deposit(String accountType, double amount, User user);

    void withdraw(String accountType, Double amount, User user);
}
