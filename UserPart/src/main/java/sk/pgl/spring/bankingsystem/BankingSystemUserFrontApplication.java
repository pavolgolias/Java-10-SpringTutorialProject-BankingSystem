package sk.pgl.spring.bankingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankingSystemUserFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankingSystemUserFrontApplication.class, args);
	}
}
