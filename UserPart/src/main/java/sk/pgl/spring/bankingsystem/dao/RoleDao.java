package sk.pgl.spring.bankingsystem.dao;

import org.springframework.data.repository.CrudRepository;
import sk.pgl.spring.bankingsystem.domain.security.Role;

public interface RoleDao extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}
