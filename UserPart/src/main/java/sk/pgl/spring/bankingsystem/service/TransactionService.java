package sk.pgl.spring.bankingsystem.service;

import sk.pgl.spring.bankingsystem.domain.*;

import java.security.Principal;
import java.util.List;

public interface TransactionService {
    List<PrimaryTransaction> findPrimaryTransactionList(User user);

    List<SavingsTransaction> findSavingsTransactionList(User user);

    void savePrimaryDepositTransaction(PrimaryTransaction primaryTransaction);

    void saveSavingsDepositTransaction(SavingsTransaction savingsTransaction);

    void savePrimaryWithdrawTransaction(PrimaryTransaction primaryTransaction);

    void saveSavingsWithdrawTransaction(SavingsTransaction savingsTransaction);

    void betweenAccountsTransfer(String transferFrom,
            String transferTo,
            double amount,
            PrimaryAccount primaryAccount,
            SavingsAccount savingsAccount) throws Exception;

    List<Recipient> findRecipientList(String username);

    void saveRecipient(Recipient recipient);

    Recipient findRecipientByName(String recipientName);

    void deleteRecipientByName(String recipientName);

    void toSomeoneElseTransfer(Recipient recipient,
            String accountType,
            double amount,
            PrimaryAccount primaryAccount,
            SavingsAccount savingsAccount);
}
