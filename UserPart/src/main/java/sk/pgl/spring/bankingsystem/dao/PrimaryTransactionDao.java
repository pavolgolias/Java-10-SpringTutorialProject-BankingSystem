package sk.pgl.spring.bankingsystem.dao;

import org.springframework.data.repository.CrudRepository;
import sk.pgl.spring.bankingsystem.domain.PrimaryTransaction;

import java.util.List;

public interface PrimaryTransactionDao extends CrudRepository<PrimaryTransaction, Long> {
    List<PrimaryTransaction> findAll();
}
