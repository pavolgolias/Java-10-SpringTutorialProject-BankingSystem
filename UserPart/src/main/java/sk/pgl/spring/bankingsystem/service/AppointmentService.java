package sk.pgl.spring.bankingsystem.service;

import sk.pgl.spring.bankingsystem.domain.Appointment;

import java.util.List;

public interface AppointmentService {
    Appointment createAppointment(Appointment appointment);

    List<Appointment> findAll();

    Appointment findAppointment(Long id);

    void confirmAppointment(Long id);
}
