package sk.pgl.spring.bankingsystem.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import sk.pgl.spring.bankingsystem.domain.PrimaryTransaction;
import sk.pgl.spring.bankingsystem.domain.SavingsTransaction;
import sk.pgl.spring.bankingsystem.domain.User;
import sk.pgl.spring.bankingsystem.service.TransactionService;
import sk.pgl.spring.bankingsystem.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasRole('ADMIN')")
public class UserResource {
    private UserService userService;
    private TransactionService transactionService;

    @Autowired
    public UserResource(UserService userService, TransactionService transactionService) {
        this.userService = userService;
        this.transactionService = transactionService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<User> userList() {
        return userService.findUserList();
    }

    @RequestMapping(value = "/primary/transaction", method = RequestMethod.GET)
    public List<PrimaryTransaction> getPrimaryTransactionList(@RequestParam("username") String username) {
        User user = userService.findByUsername(username);
        return transactionService.findPrimaryTransactionList(user);
    }

    @RequestMapping(value = "/savings/transaction", method = RequestMethod.GET)
    public List<SavingsTransaction> getSavingsTransactionList(@RequestParam("username") String username) {
        User user = userService.findByUsername(username);
        return transactionService.findSavingsTransactionList(user);
    }

    @RequestMapping("/{username}/enable")
    public void enableUser(@PathVariable("username") String username) {
        userService.enableUser(username);
    }

    @RequestMapping("/{username}/disable")
    public void disableUser(@PathVariable("username") String username) {
        userService.disableUser(username);
    }
}
