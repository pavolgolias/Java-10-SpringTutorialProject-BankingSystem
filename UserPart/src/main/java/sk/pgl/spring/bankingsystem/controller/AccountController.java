package sk.pgl.spring.bankingsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sk.pgl.spring.bankingsystem.domain.*;
import sk.pgl.spring.bankingsystem.service.AccountService;
import sk.pgl.spring.bankingsystem.service.TransactionService;
import sk.pgl.spring.bankingsystem.service.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/account")
public class AccountController {
    private UserService userService;
    private AccountService accountService;
    private TransactionService transactionService;

    @Autowired
    public AccountController(UserService userService,
            AccountService accountService,
            TransactionService transactionService) {
        this.userService = userService;
        this.accountService = accountService;
        this.transactionService = transactionService;
    }

    @RequestMapping("/primaryAccount")
    public String primaryAccount(Model model, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        PrimaryAccount primaryAccount = user.getPrimaryAccount();
        List<PrimaryTransaction> transactions = transactionService.findPrimaryTransactionList(user);

        model.addAttribute("primaryAccount", primaryAccount);
        model.addAttribute("primaryTransactionList", transactions);

        return "primaryAccount";
    }

    @RequestMapping("/savingsAccount")
    public String savingsAccount(Model model, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        SavingsAccount savingsAccount = user.getSavingsAccount();
        List<SavingsTransaction> transactions = transactionService.findSavingsTransactionList(user);

        model.addAttribute("savingsAccount", savingsAccount);
        model.addAttribute("savingsTransactionList", transactions);

        return "savingsAccount";
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.GET)
    public String deposit(Model model) {
        model.addAttribute("accountType", "");
        model.addAttribute("amount", "");

        return "deposit";
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    public String depositPost(@ModelAttribute("amount") Double amount,
            @ModelAttribute("accountType") String accountType,
            Principal principal) {

        User user = userService.findByUsername(principal.getName());
        accountService.deposit(accountType, amount, user);

        return "redirect:/userFront";
    }

    @RequestMapping(value = "/withdraw", method = RequestMethod.GET)
    public String withdraw(Model model) {
        model.addAttribute("accountType", "");
        model.addAttribute("amount", "");

        return "withdraw";
    }

    @RequestMapping(value = "/withdraw", method = RequestMethod.POST)
    public String withdrawPost(@ModelAttribute("amount") Double amount,
            @ModelAttribute("accountType") String accountType,
            Principal principal) {

        User user = userService.findByUsername(principal.getName());
        accountService.withdraw(accountType, amount, user);

        return "redirect:/userFront";
    }
}
