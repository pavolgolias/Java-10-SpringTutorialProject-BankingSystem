package sk.pgl.spring.bankingsystem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.pgl.spring.bankingsystem.dao.AppointmentDao;
import sk.pgl.spring.bankingsystem.domain.Appointment;
import sk.pgl.spring.bankingsystem.service.AppointmentService;

import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {
    private final AppointmentDao appointmentDao;

    @Autowired
    public AppointmentServiceImpl(AppointmentDao appointmentDao) {
        this.appointmentDao = appointmentDao;
    }

    @Override
    public Appointment createAppointment(Appointment appointment) {
        return appointmentDao.save(appointment);
    }

    @Override
    public List<Appointment> findAll() {
        return appointmentDao.findAll();
    }

    @Override
    public Appointment findAppointment(Long id) {
        return appointmentDao.findOne(id);
    }

    @Override
    public void confirmAppointment(Long id) {
        Appointment appointment = appointmentDao.findOne(id);
        appointment.setConfirmed(true);
        appointmentDao.save(appointment);
    }
}
