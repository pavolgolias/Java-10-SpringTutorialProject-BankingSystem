package sk.pgl.spring.bankingsystem.dao;

import org.springframework.data.repository.CrudRepository;
import sk.pgl.spring.bankingsystem.domain.Appointment;

import java.util.List;

public interface AppointmentDao extends CrudRepository<Appointment, Long> {

    List<Appointment> findAll();
}
