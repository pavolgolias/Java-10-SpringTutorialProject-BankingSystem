﻿## Spring Tutorial Project - Online Banking System 
> This project contains an example of Online Banking system implemented using Spring and Angular 2.

**Project folders description:**
 - _Specification_ - contains pictures and documents describing the features of project
 - _AdminPortal_ - contains admin portal project part implemented using Angular 2, which is using consuming REST API implemented in project in _UserPart_ folder
 - _AdminPortalDemoServer_ - Spring Boot project used as demo deployment environment for AdminPortal
 - _UserPart_ - complete Spring project providing all business logic, DB connection, MVC with Thymeleaf view for common user and REST API for admin
 
 **Used technologies:**
 - [Spring](https://spring.io/) - Spring Boot, Spring Data, Hibernate, SQL, Spring Security, Spring MVC, REST, Thymeleaf, Maven, HTML, JS, CSS
 - [Angular](https://angular.io/), [NPM](https://www.npmjs.com/), [Node.js](https://nodejs.org/en/), [Angular CLI](https://github.com/angular/angular-cli), [TypeScript](https://www.typescriptlang.org/) 

